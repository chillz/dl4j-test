package nd4j.test;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

/**
 *   @Author Liuyang
 *   @Email 1670094472@qq.com
 *   @Date 2020/1/16 20:38
 *   @Description : 创建NDArray测试
 */
public class CreateND4j {

    public static void main(String[] args) {

        //创建一个三行三列的全0的NDArray
        final INDArray indArray = Nd4j.zeros(3,3);
        System.out.println(indArray);

        //创建一个三行三列的全是1的NDArray
        final INDArray indArray1 = Nd4j.ones(3,3);
        System.out.println(indArray1);

        //构造一个3行5列，数组元素均为随机产生的ndarray
        final INDArray indArray2 = Nd4j.rand(3, 5);
        System.out.println(indArray2);

        //构造一个3行5列，数组元素服从高斯分布（平均值为0，标准差为1）的ndarray
        //构造一个3行5列，数组元素服从高斯分布（平均值为0，标准差为1）的ndarray
        final INDArray indArray3 = Nd4j.randn(3, 5);
        System.out.println(indArray3);

        //给一个一维数据，根据shape创造NDArray
        INDArray array1 = Nd4j.create(new float[]{2, 2, 2, 2}, new int[]{1, 4});
        System.out.println(array1);
        INDArray array2 = Nd4j.create(new float[]{2, 2, 2, 2}, new int[]{2, 2});
        System.out.println(array2);

        final INDArray mul = indArray.mul(3);
        final INDArray mul1 = indArray3.mul(indArray2);
        System.out.println(mul);
        System.out.println(mul1);

    }
}
